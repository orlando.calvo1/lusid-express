import re
import inspect
from typing import Any, Dict, Optional

def camel_to_snake(name):
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()

def generate_api_client_classes(api_module, output_file):

    with open(output_file, 'w') as f:

        for attr_name in dir(api_module):
            attr = getattr(api_module, attr_name)
            if isinstance(attr, type) and attr_name.endswith('Api'):
                object_name = camel_to_snake(attr_name[:-3]) + '_api'
                f.write(f"class __{attr_name}AuthedAndStubbed:\n")
                f.write("   from lusid.utilities import ApiClientFactory as __ApiClientFactory\n")
                f.write("   from lusidjam import RefreshingToken as __RefreshingToken\n")
                f.write("   from typing import Dict, Any\n")
                f.write("   import os as __os\n")
                f.write("   __api_factory = __ApiClientFactory(token=__RefreshingToken(), api_secrets_filename=__os.environ.get('FBN_SECRETS_PATH',None), app_name='LusidJupyterNotebook')\n\n")

                f.write("   def __init__(self):\n")
                f.write("       import lusid.api as __api\n")
                f.write(f"       self.__client = self.__api_factory.build(__api.{attr_name})\n\n")

                if callable(attr):
                    for method_name in dir(attr()):
                        method = getattr(attr(), method_name)
                        if callable(method) and not method_name.startswith('_'):
                            sig = inspect.signature(method)
                            param_str_list = []  # Initialize an empty list to store parameter strings
                            param_names = []  # List to store parameter names for the method call
                            for p in sig.parameters.values():
                                if p.kind == inspect.Parameter.VAR_KEYWORD:
                                    # Add **kwargs to the method signature
                                    param_str_list.append(f"**{p.name}")
                                    # Handle **kwargs separately in the method call
                                    param_names.append('**kwargs')
                                    continue
                                # Form the parameter string for non-VAR_KEYWORD parameters
                                param_type = 'Dict' if p.annotation == dict else (p.annotation.__name__ if p.annotation != inspect.Parameter.empty else 'Any')
                                default_value = " = Any" if p.default != inspect.Parameter.empty else ''
                                param_str_list.append(f"{p.name}: {param_type}{default_value}")
                                param_names.append(p.name)  # Add the parameter name for the method call

                            # Join all parameter strings from the list into one comma-separated string
                            param_str = ', '.join(param_str_list)

                            docstring = inspect.getdoc(method) or "No documentation available."
                            docstring = inspect.cleandoc(docstring)  # Clean and format the docstring
                            f.write(f"   def {method_name}(self, {param_str}) -> 'Any':\n")
                            f.write(f'        """{docstring}"""\n')
                            # Use param_names, which includes '**kwargs' where appropriate
                            f.write(f"        return self.__client.{method_name}({', '.join(param_names)})\n\n")

                    f.write(f"{object_name} = __{attr_name}AuthedAndStubbed()\n\n")


if __name__ == "__main__":
    import lusid.api as apis  
    generate_api_client_classes(apis, 'src/lusid_express/apis/__init__.py')
