import os
import subprocess
import sys
from typing import Dict, List
import os as __os

def load()->Dict[str,List[str]]:
    import yaml as __yaml
    dirname = __os.path.dirname(__file__)
    config_path = 'src/lusid_express/config.yaml'
    if not __os.path.exists(config_path):
        return {}
    with open(config_path, 'r') as f:
        return __yaml.safe_load(f) or {}
    
def test_cli():
    os.system("rm -rf src/lusid_express/config.yaml")
    #run the python -m lusid_express --enable vars command
    command = [sys.executable, "src/lusid_express/__main__.py", "--enable", "vars"]
    result = subprocess.run(command, capture_output=True, text=True)
    print("stdout:", result.stdout)
    print("stderr:", result.stderr)
    #check if the config.yaml file has been created with the vars feature enabled
    assert os.path.exists("src/lusid_express/config.yaml")
    config = load()
    print(config)
    assert 'vars' in config['features'] 
    command = [sys.executable, "src/lusid_express/__main__.py", "--disable", "vars"]
    subprocess.run(command, capture_output=True, text=True)
    config = load()
    assert 'vars' not in config['features']
    command = [sys.executable,  "src/lusid_express/__main__.py", "--enable", "vars", "magic"]
    subprocess.run(command, capture_output=True, text=True)
    config = load()
    assert 'vars' in config['features']
    assert 'magic' in config['features']
    command = [sys.executable,  "src/lusid_express/__main__.py", "--disable", "vars",  "magic"]
    subprocess.run(command, capture_output=True, text=True)
    config = load()
    assert 'vars' not in config['features']
    assert 'magic' not in config['features']

        