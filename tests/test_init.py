

def test_init():
    from os import environ
    try:
        import lusid_express
        assert True  # If import succeeds, pass the test
    except Exception as e:  # Catching a more general exception, or specify the exact type you expect
        error_message = str(e)
        expected_message = (
            "The fields ['api_url', 'password', 'username', 'client_id', "
            "'client_secret', 'token_url']"
        )
        if expected_message in error_message:
            assert True  # Pass test with this expected error
        else:
            assert False, f"Unexpected error message: {error_message}"  # Fail if the error is anything else
