This IPython session has already instantiated the following objects
| Variable Name | Statement         | Description                          |
|---------------|-------------------|--------------------------------------|
| lu          | `import lusid as lu` | Main LUSID package                   |
| lm          | `import lusid.models as lm` | LUSID models module                  |
| apis         | `import lusid_express.apis as apis` | convenience package providing pre-authenticated api access.                 |
